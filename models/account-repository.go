package models

import (
	"gitlab.com/tsatsubii/tsatsubii-service"

	"gitlab.com/tsatsubii/tsatsubii-blockonomics/forms"
)

type AccountRepository struct{}

func (r *AccountRepository) Create(form forms.AccountForm) (Account, error) {
	account := Account{
		Name:   form.Name,
		ApiKey: form.ApiKey,
	}

	if err := account.Validate(); err != nil {
		return Account{}, err
	}

	if err := Db.Create(&account).Error; err != nil {
		err := tsatsubii.InternalError{}
		return Account{}, &err
	}

	return account, nil
}

func (r *AccountRepository) Exists(name string) bool {
	var count int64

	result := Db.Model(
		&Account{},
	).Where(
		"name = ?",
		name,
	).Count(
		&count,
	)

	if result.Error != nil {
		return false
	}

	return count > 0
}

func (r *AccountRepository) ApiKeyExists(key string) bool {
	var count int64

	result := Db.Model(
		&Account{},
	).Where(
		"api_key = ?",
		key,
	).Count(
		&count,
	)

	if result.Error != nil {
		return false
	}

	return count > 0
}
