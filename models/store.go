package models

import (
	"github.com/google/uuid"
	"gitlab.com/tsatsubii/tsatsubii-service"
)

type Store struct {
	tsatsubii.Model
	Name       string    `gorm:"unique" json:"name"`
	AccountId  uuid.UUID `json:"account_id"`
	CallbackId string    `gorm:unique" json:"callback_id"`
}

type Stores []Store
