package models

import (
	"gitlab.com/tsatsubii/tsatsubii-service"
)

type Account struct {
	tsatsubii.Model
	Name   string `gorm:"unique" json:"name"`
	ApiKey string `gorm:"unique" json:"api_key"`
	Stores Stores `json:"stores"`
}

type Accounts []Account

func (a *Account) Validate() error {
	err := tsatsubii.ValidationError{}

	if !a.ValidateName() {
		err.AddError("invalid account name")
	}

	if !a.ValidateApiKey() {
		err.AddError("invalid API key")
	}

	repo := AccountRepository{}
	if repo.Exists(a.Name) {
		err.AddError("an account with that name already exists")
	}

	if repo.ApiKeyExists(a.ApiKey) {
		err.AddError("an account with that API key already exists")
	}

	if err.HasErrors() {
		return &err
	}

	return nil
}

func (a *Account) ValidateName() bool {
	return true
}

func (a *Account) ValidateApiKey() bool {
	return true
}
