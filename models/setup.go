package models

import (
	"gitlab.com/tsatsubii/tsatsubii-service"
	"gorm.io/gorm"
)

const (
	PermManageBlockonomics tsatsubii.PermissionKey = "manage blockonomics"
)

var Db *gorm.DB

func Setup() {
	Db = tsatsubii.Database()

	Db.AutoMigrate(&Account{})
	Db.AutoMigrate(&Store{})

	tsatsubii.GrantPermission(
		tsatsubii.SysAdminRoleID(),
		PermManageBlockonomics,
	)
}
