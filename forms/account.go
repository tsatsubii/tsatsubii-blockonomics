package forms

type AccountForm struct {
	Name   string `json:"name" binding:"required"`
	ApiKey string `json:"api_key" binding:"required"`
}
