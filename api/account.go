package api

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/tsatsubii/tsatsubii-blockonomics/forms"
	"gitlab.com/tsatsubii/tsatsubii-blockonomics/models"
)

func CreateAccount(c *gin.Context) {
	var form forms.AccountForm

	if err := c.ShouldBindJSON(&form); err != nil {
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"error": err.Error(),
			},
		)

		return
	}

	accountRepo := models.AccountRepository{}
	account, err := accountRepo.Create(form)
	if err != nil {
		c.JSON(
			http.StatusBadRequest,
			gin.H{
				"error": err.Error(),
			},
		)

		return
	}

	c.JSON(
		http.StatusOK,
		gin.H{
			"message":    "success",
			"account_id": account.ID,
		},
	)
}
