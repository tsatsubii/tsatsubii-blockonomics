package main

import (
	"gitlab.com/tsatsubii/tsatsubii-service"

	"gitlab.com/tsatsubii/tsatsubii-blockonomics/api"
	"gitlab.com/tsatsubii/tsatsubii-blockonomics/models"
)

func main() {
	tsatsubii.Init()
	models.Setup()

	tsatsubii.AddAuthRoute(
		"POST",
		"/accounts",
		models.PermManageBlockonomics,
		api.CreateAccount,
	)

	tsatsubii.Start()
}
