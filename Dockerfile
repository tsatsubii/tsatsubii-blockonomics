FROM golang:alpine

RUN apk update && apk add --no-cache git
RUN mkdir /app
WORKDIR /app

COPY . .
RUN go get
RUN go build

CMD ["/app/tsatsubii-blockonomics"]
